CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation and configuration
 * Maintainers

 INTRODUCTION
 ------------

 The Commerce LiqPayAPI module provides Liqpay payment through LiqPay API v.3 (liqpay.com), when using Drupal Commerce.
 LIQPAY, the easiest payments for your site or mobile app!
 Supported payment methods:
 - card (Master Card or Visa)
 - Privat 24 internet banking
 - LiqPay account
 - Delayed
 - Invoice

  * For a full description of the module, visit the project page:
    https://drupal.org/project/commerce_liqpayapi

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/issues/commerce_liqpayapi

 REQUIREMENTS
 ------------

 This module requires the following modules:

  * Drupal Commerce (https://www.drupal.org/project/commerce)

 INSTALLATION and CONFIGURATION
 ------------------------------

 Сonnecting to the system LiqPay and setting up

 1. Go to the site www.liqpay.com and sign up.

 2. Create an account store to accept payments.

 3. Save "Public Key" and "Private Key", and check option "POST Data" on the tab API in the BUSINESS section

 Installation & configuring

 1. Install module as usual:
 - Place this module directory in your modules folder (this will usually be "sites/all/modules/");
 - Go to Administer -> Modules and enable the module;
 - Flush caches.

 2. Сonfiguration
 - Go to the page Administration » Configuration » Payment methods;
 - Find "LiqPay API 3.0" and click the edit link;
 - In action find: "Enable payment method: LiqPay API 3.0" and click edit link;
 - Under "Payment settings", you can configure the payment details:
 a) Public key - the store identifier. The key can be received in the shop settings;
 b) Private key;
 c) Pay Way - Available user payment methods;
 d) Option to enable payment in test mode (sandbox = 1);
 e) Payment Statuses for LiqPay response;
 f) Customizing status messages of payments for users;
 g) Set up redirect path.

 3. Use and enjoy!!!

 MAINTAINERS
 -----------

 Current maintainers:
  * Alexandr Cherniy (blackiceua) - https://www.drupal.org/user/3076235